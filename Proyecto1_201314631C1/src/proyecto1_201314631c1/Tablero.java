
package proyecto1_201314631c1;

import java.awt.Color;
import java.awt.Rectangle;
 import java.util.Random;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class Tablero extends javax.swing.JFrame {

//public static Personaje heroe=null;
public static Objeto meta=null;
public static Fondo fondoactual=null;
//public static ArrayList<Personaje>villanos= new ArrayList<Personaje>();
public static ArrayList<JLabel>villetiquetas= new ArrayList<JLabel>();
public static ArrayList<Objeto>ObjetosJuego= new ArrayList<Objeto>();
public static ArrayList<JLabel>objetiquetas= new ArrayList<JLabel>();
public static int ancho=0;
public static int alto=0;
public static boolean activo=true;
public static String ultflecha="derecha";
public static JLabel armaactual=null;
public static int dano=0;
  JPanel jpanel = new JPanel();
 public static JList list=new JList();
 Border border = BorderFactory.createLineBorder(Color.black, 1);   
 //JLabel[] lblvillanos = new JLabel[villanos.size()];
 JLabel[] lblobjetos = new JLabel[ObjetosJuego.size()];
 JLabel lblheroe= new JLabel();
 JLabel lblmeta= new JLabel();
 JLabel des= new JLabel();
 
  public static int heroex=0;
  public static int heroey=0;
   /* static void Perdio()
    {
    if (heroe.vida<=0)
    {
    activo=false;
    JOptionPane.showMessageDialog(null, "Lo sentimos el juego ah terminado, as perdido!!!!");
    }
    }*/
 void GenerarTablero()
 {
     

     jpanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
     jpanel.setBounds(new Rectangle(0,0,50*ancho,50*alto));
     jpanel.setLayout(null);
     
    /*
              int i=0;
               for (Personaje v:villanos) {
                    lblvillanos[i] = new JLabel(); 
                    lblvillanos[i].setIcon (new javax.swing.ImageIcon(v.PathImagen)); 
                     lblvillanos[i].setBounds(new Rectangle(v.PosX*50, v.PosY*50, 50, 50));
                       jpanel.add(lblvillanos[i], null);
                       villetiquetas.add(lblvillanos[i]);
                       i++;
                   // lblvillanos[i].setHorizontalAlignment(SwingConstants.CENTER);
               }*/
               int j=0;
               for (Objeto o:ObjetosJuego) {
                    lblobjetos[j] = new JLabel(); 
                    lblobjetos[j].setIcon (new javax.swing.ImageIcon(o.Path)); 
                     lblobjetos[j].setBounds(new Rectangle(o.getX()*50, o.getY()*50, 50, 50));
                       jpanel.add(lblobjetos[j], null);
                       objetiquetas.add(lblobjetos[j]);
                       j++;
                   // lblvillanos[i].setHorizontalAlignment(SwingConstants.CENTER);
               }/*
               lblheroe.setIcon (new javax.swing.ImageIcon(heroe.PathImagen)); 
                     lblheroe.setBounds(new Rectangle(heroe.PosX*50, heroe.PosY*50, 50, 50));
                       jpanel.add(lblheroe, null);
                       
                lblmeta.setIcon (new javax.swing.ImageIcon(meta.Path)); 
                     lblmeta.setBounds(new Rectangle(meta.getX()*50, meta.getY()*50, 50, 50));
                       jpanel.add(lblmeta, null);
                  */     
                
                       
               this.getContentPane().add(jpanel,null);
               jpanel.repaint();
                if (fondoactual!=null)
                {
                        jLabel1.setIcon (new javax.swing.ImageIcon(fondoactual.PathFondo)); 
                        jLabel1.setBounds(new Rectangle(0, 0,50*ancho,50*alto));
                        jpanel.add(jLabel1, null);
                }
                         jpanel.repaint();
             // MovimientoEnemigos();
              des.setBounds(new Rectangle((50*ancho)+20, 70,100,30));
              des.setText("DESCRIPCION DEL JUEGO");
              des.setVisible(true);
              list.setBounds(new Rectangle((50*ancho)+20, 100,200,600));
              //list.setLocation(ancho+20, 100);
              list.setLayout(null);
              list.setVisible(true);
              list.enable(false);
              this.getContentPane().add(list,null);
              this.getContentPane().add(des,null);
              //ActualizarLista();
               jpanel.repaint();
               FormularioConfiguracion.TablaSimbolos(2);
 }
/*static void ActualizarLista()
 {
     DefaultListModel modelo = new DefaultListModel();
    // modelo.addElement(heroe.nombre+" vida: "+heroe.vida);
     
 for (Personaje v:villanos)
 {
 modelo.addElement(v.nombre+" vida: "+v.vida);
 }
 list.setModel(modelo);
 }*/
/*
 void MovimientoEnemigos()
 {
     Random rnd= new Random();
     int i=0;
         for (Personaje v:villanos) //recorro todos los villanos que tengo
            {
                int num=(rnd.nextInt(4)+1);        
                int vel=(rnd.nextInt(20)+2);        
                Figura villano = new Figura();
                villano.et=lblvillanos[i];
                villano.tipo="villano";
                villano.direccion=num;
                villano.velocidad=vel;
                villano.start();
             i++;
            }
 }*/
 /*public static boolean ExistePared(int x, int y,int op) //op =1 es el heroe
 {
     boolean respuesta=false;
     int i=0;
     if (op==3)
     {
         
            if (x==heroex && y==heroey)
            {
                //JOptionPane.showMessageDialog(null, "le pegaron");
                int dano=0;
                int j=0;
                for (JLabel p:villetiquetas)
                {
                    if(villetiquetas.get(j).isVisible()==true)
                    {
                        if (p.getX()+50==x && p.getY()==y)
                    dano=villanos.get(j).destruccion;
                else if(p.getX()==x && p.getY()+50==y)
                    dano=villanos.get(j).destruccion;
                    else if(p.getX()-50==x && p.getY()==y)
                        dano=villanos.get(j).destruccion;
                        else if(p.getX()==x && p.getY()-50==y)
                            dano=villanos.get(j).destruccion;
                    }
                
                    j++;
                }
                
                heroe.vida=heroe.vida-dano;
                ActualizarLista();
                Perdio();
                
            }
        
     }
     
     for (Objeto o:ObjetosJuego)
     {
     if (o.tipo.equals("bloque"))
     {
         if ((o.getX()*50)==x && (o.getY()*50)==y)
         {
         respuesta=true;
         return respuesta;
         } 
     }
     else if (o.tipo.equals("arma") && op==1)
     {
         if ((o.getX()*50)==x && (o.getY()*50)==y && objetiquetas.get(i).isVisible()==true)
         {
             objetiquetas.get(i).setVisible(false);
             armaactual=objetiquetas.get(i);
             dano=o.destruir;
             respuesta=false;
         }
     
     }
     else if (o.tipo.equals("bonus") && op==1)
     {
         if ((o.getX()*50)==x && (o.getY()*50)==y && objetiquetas.get(i).isVisible()==true)
         {
             int bono=o.creditos;
             heroe.vida=heroe.vida+bono;
             objetiquetas.get(i).setVisible(false);
             ActualizarLista();
         }
     
     }
     else if (o.tipo.equals("bomba") && op==1)
     {
         if ((o.getX()*50)==x && (o.getY()*50)==y && objetiquetas.get(i).isVisible()==true)
         {
             int bomb=o.destruir;
             heroe.vida=heroe.vida-bomb;
             objetiquetas.get(i).setVisible(false);
             ActualizarLista();
             Perdio();
         }
     
     }
     i++;
     }
     return respuesta;
 }
 public static boolean DisparoPosXPosY(int x, int y, int dano)
 {
     boolean res=false; ///este metodo devulve true si le pego a algo
     int i=0;
     for (JLabel villano:villetiquetas)
     {
         if ((villano.getX())==x && (villano.getY())==y) //si le pego
         {
             if (villetiquetas.get(i).isVisible())
             {
                 //JOptionPane.showMessageDialog(null, "le dio");
             int vid=villanos.get(i).vida;
             villanos.get(i).vida=vid-dano;
             
             if (villanos.get(i).vida <=0)
             {
                 villanos.get(i).vida=0;
                 villetiquetas.get(i).setVisible(false);
                 
             }
             ActualizarLista();
             res=true;
             }
         } 
     
     i++;
     }
     return res;
 }*/
 
    public Tablero() {
        initComponents();
 GenerarTablero();
    }

 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 955, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 452, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 497, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(319, 319, 319))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        /*int x=lblheroe.getX();
        int y=lblheroe.getY();
         if (evt.getKeyChar()=='a' || evt.getKeyChar()=='s' || evt.getKeyChar()=='d' || evt.getKeyChar()=='w' ) //disparar bolita generar nueva
         {
            if (armaactual!=null)
            {
                
            JLabel bolita= new JLabel();
            Bolita nueva= new Bolita();
            bolita=armaactual;
            bolita.setVisible(true);
            nueva.et=bolita;
            nueva.dano=dano;
            switch (evt.getKeyChar())
            {
                case 'a':
                    nueva.direccion="izquierda";
                    break;
                case 's':
                    nueva.direccion="abajo";
                    break;
                case 'd':
                    nueva.direccion="derecha";
                    break;
                case 'w':
                    nueva.direccion="arriba";
                    break;
            }
            nueva.start();
            }   
         }
        if(evt.getKeyCode()==38)
        {
            if (heroee.iniciado==true)
            {
                   heroee.direccion=3;     
               heroee.resume();
            }
            else
            {
                 heroee.direccion=3;
                heroee.iniciado=true;
                heroee.start();
            }
             
            
        }
        if(evt.getKeyCode()==40)
        {
            if (heroee.iniciado==true)
            {
                   heroee.direccion=4;     
               heroee.resume();
            }
            else
            {
                 heroee.direccion=4;
                heroee.iniciado=true;
                heroee.start();
            }
            
        }
        if(evt.getKeyCode()==37)
        {            
            if (heroee.iniciado==true)
            {
                   heroee.direccion=2;     
                   heroee.resume();
            }
            else
            {
                 heroee.direccion=2;
                heroee.iniciado=true;
                heroee.start();
            }
            
            
        }
        if(evt.getKeyCode()==39)
        {
            if (heroee.iniciado==true)
            {
                   heroee.direccion=1;     
               heroee.resume();
            }
            else
            {
                 heroee.direccion=1;
                heroee.iniciado=true;
                heroee.start();
            }
            
            
        }*/
    }//GEN-LAST:event_formKeyPressed

   
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Tablero.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Tablero.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Tablero.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Tablero.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Tablero().setVisible(true);
            }
            
        });
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
