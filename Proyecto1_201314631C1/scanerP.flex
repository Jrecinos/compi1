package proyecto1_201314631c1;

import java.io.FileReader;
import java.io.File;
import java.io.FileWriter;
      import java.io.Reader;
	  import java.io.IOException;
import java.io.BufferedWriter;
import java_cup.runtime.*;
import java.io.Reader;
import java.util.ArrayList;
      
%% 
   

%class AnalizadorLexico

%line
%column
    
%unicode
%cup
%state STRING
%state STRINGDIRE
%ignorecase

%{
   
			
               
	
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    

    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
	String linea="";
	String dire="";
	String identificador="";
	String let="";
	String texto="";
	
	void AddError(int fila,int columna, String error)
	{
		ErroresC nuevo= new ErroresC(fila,columna,"Caracter No valido",error,"Lexico");
		FormularioConfiguracion.Errs.add(nuevo);
	}
%}   

Inconfig="<configuracion>"
Finconfig="</configuracion>"
Inback="<fondo>"
Finback="</fondo>"
Indesign="<disenio>"
Findesign="</disenio>"
nombre="nombre"
path="path"
tipo="tipo"
puntos="puntos"
crecimiento="crecimiento"
tiempo="tiempo"
bonus="bonus"
manzana="manzana"
bomba="bomba"
bloque="bloque"	
estrella="estrella"
veneno="veneno"




Inparedes="<paredes>"
Finparedes="</paredes>"
Inextras="<extras>"
Finextras="</extras>"
InManza="<manzana>"
FinManza="</manzana>"
InVene="<veneno>"
FinVene="</veneno>"
InBomb="<bomba>"
FinBomb="/bomba>"
InEstr="<estrella>"
FinEstr="</estrella>"


Inpersonajes="<Personajes>"
Finpersonajes="</Personajes>"
Inheroes="<heroes>"
Finheroes="</heroes>"
Invillanos="<villanos>"
Finvillanos="</villanos>"

Inarmas="<armas>"
Finarmas="</armas>"
Inbonus="<bonus>"
Finbonus="</bonus>"


Infigure="<figure>"
Finfigure="</figure>"

Inmeta="<meta>"
Finmeta="</meta>"
InEsce="<escenario " 
PalabraBack="fondo" 
PalabraAncho="ancho"
PalabraAlto="alto"
Finescenarios="</escenario>"

x_imagen="x-imagen"
x_tipo="x-tipo"
x_vida="x-vida"
x_destruir="x-destruir"



x_arma="x-arma"



dir={letra} ":\\" ["."a-zA-Z0-9"\\"]*
id={letra}[a-zA-Z0-9"_"]*
PuntoPunto=".""."
letra = [a-zA-Z]
entero= [0-9]+
numero = [0-9]
Salto = \r|\n|\r\n   
Espacio     = {Salto} | [ \t\f]

%%
<YYINITIAL> {nombre}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.Nombre);}
<YYINITIAL> {path}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.Path);}
<YYINITIAL> {tipo}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.Tipo);}
<YYINITIAL> {puntos}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.Puntos);}
<YYINITIAL> {crecimiento}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.Crecimiento);}
<YYINITIAL> {tiempo}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.Tiempo);}
<YYINITIAL> {bonus}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.Bonus);}
<YYINITIAL> {manzana}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.Manzana);}
<YYINITIAL> {bomba}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.Bomba);}
<YYINITIAL> {bloque}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.Bloque);}
<YYINITIAL> {estrella}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.Estrella);}
<YYINITIAL> {veneno}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.Veneno);}

<YYINITIAL> {InManza}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.ManzaIN);}
<YYINITIAL> {FinManza}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.ManzaFIN);}
<YYINITIAL> {InVene}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.VeneIN);}
<YYINITIAL> {FinVene}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.VeneFIN);}
<YYINITIAL> {InBomb}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.BombIN);}
<YYINITIAL> {FinBomb}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.BombFIN);}
<YYINITIAL> {InEstr}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.EstrIN);}
<YYINITIAL> {FinEstr}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.EstrFIN);}






<YYINITIAL> {Inpersonajes}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.PersonajesIN);}
<YYINITIAL> {Finpersonajes}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.PersonajesFIN);}
<YYINITIAL> {Inheroes}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.HeroesIN);}
<YYINITIAL> {Finheroes}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.HeroesFIN);}
<YYINITIAL> {Invillanos}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.VillanoIN);}
<YYINITIAL> {Finvillanos}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.VillanoFIN);}
<YYINITIAL> {Inparedes}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.ParedIN);}
<YYINITIAL> {Finparedes}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.ParedFIN);}
<YYINITIAL> {Inextras}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.ExtrasIN);}
<YYINITIAL> {Finextras}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.ExtrasFIN);}
<YYINITIAL> {Inarmas}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.ArmasIN);}
<YYINITIAL> {Finarmas}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.ArmasFIN);}
<YYINITIAL> {Inbonus}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.BonusIN);}
<YYINITIAL> {Finbonus}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.BonusFIN);}
<YYINITIAL> {Inconfig}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.configIN);}
<YYINITIAL> {Finconfig}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.configFIN);}
<YYINITIAL> {Inback}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.backgrIN);}
<YYINITIAL> {Finback}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.backgrFIN);}
<YYINITIAL> {Infigure}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.figIN);}
<YYINITIAL> {Finfigure}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.figFin);}
<YYINITIAL> {Indesign}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.designIN);}
<YYINITIAL> {Findesign}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.designFIN);}

<YYINITIAL> {x_imagen}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.ximagen);}
<YYINITIAL> {x_tipo}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.xtipo);}
<YYINITIAL> {x_vida}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.xvida);}
<YYINITIAL> {x_destruir}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.xdestruir);}


<YYINITIAL> {entero}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.entero,new String (yytext() ) );}
<YYINITIAL> {PuntoPunto}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.puntopunto);}
<YYINITIAL> {Inmeta}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.metaIN);}
<YYINITIAL> {Finmeta}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.metaFIN);}
<YYINITIAL> {InEsce}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.EsceIn);}
<YYINITIAL> {PalabraBack}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.palBack);}
<YYINITIAL> {PalabraAlto}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.palAlto);}
<YYINITIAL> {PalabraAncho}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.palAncho);}
<YYINITIAL> {Finescenarios}  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.escenariosFIN);}
<YYINITIAL> "("  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.parentesisabrir);}
<YYINITIAL> "<"  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.menorque);}
<YYINITIAL> ">"  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.mayorque);}
<YYINITIAL> ")"  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.parentesiscierre);}
<YYINITIAL> "["  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.llaveabrir);}
<YYINITIAL> "]"  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.llavecierre);}
<YYINITIAL> "="  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.igual);}
<YYINITIAL> ","  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.coma);}
<YYINITIAL> ";"  { System.out.println("Lexico hallamos "+yytext()); return symbol(sym.puntocoma);}
<YYINITIAL> {id}  { System.out.println("Lexico hallamos id"+yytext());  return  symbol(sym.tid,new String (yytext()));}
<YYINITIAL>    {Espacio}       { /* ignora el espacio */ } 
<YYINITIAL>  "\"" {yybegin(STRING); linea = ""; dire=""; }           
<STRING> {dir} {yybegin(STRINGDIRE); dire=yytext(); }
<STRINGDIRE> "\"" {yybegin(YYINITIAL); System.out.println("ANALISIS LEXICO SE HALLO DIRECCION "+dire);  return symbol(sym.direccionn, dire);}
 <STRING>	"\"" {yybegin(YYINITIAL); 	System.out.println("ANALISIS LEXICO SE HALLO CADENA "+linea); return symbol(sym.CADENA, linea);}
<STRING> [^"\""] {  linea = linea + yytext(); }
 
	
[^]                    { System.out.println("CARACTER NO VALIDO "+yytext()); AddError(yyline,yycolumn,yytext());

   }

//String u="<TD WIDTH=100>"   +   yytext() +    " </TD>         <TD WIDTH=100>  " + yyline+ "   </TD>              <TD WIDTH=100>   "+ yycolumn + "     </TD>        </TR>";
//s.add(u);
