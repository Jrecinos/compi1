package proyecto1_201314631c1;

import java.util.ArrayList;
public class Fondo {
public static ArrayList<Fondo>Fondos= new ArrayList<Fondo>();
String nombre;
String PathFondo;

public Fondo(String nombre, String PathFondo)
{
    this.nombre=nombre;
    this.PathFondo=PathFondo;
}

public static void SetFondo(String fondo)
{
    boolean encontrado=false;
                for (Fondo f:Fondo.Fondos)
			{
			if (f.nombre.equals(fondo))
                        {
                            Tablero.fondoactual=f;
                            encontrado=true;
                        }
			}
                if (encontrado==false)
                {
                        ErroresC nuevo= new ErroresC("Fondo no ah sido declarado",fondo,"Semantico");
                        FormularioConfiguracion.Errs.add(nuevo);
                }
}
}
