package proyecto1_201314631c1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import static proyecto1_201314631c1.Tablero.fondoactual;


public class FormularioConfiguracion extends javax.swing.JFrame {
public static ArrayList<ErroresC>Errs= new ArrayList<ErroresC>();


    public FormularioConfiguracion() {
        initComponents();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu3 = new javax.swing.JMenu();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        editor = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        editorerrores = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lblimagen = new javax.swing.JLabel();
        combo = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        lbldetalles = new javax.swing.JLabel();
        lbl3 = new javax.swing.JLabel();
        lbl2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        editorsimbolos = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();

        jMenu3.setText("jMenu3");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setFocusable(false);

        jTabbedPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        editor.setColumns(20);
        editor.setRows(5);
        jScrollPane1.setViewportView(editor);

        editorerrores.setColumns(20);
        editorerrores.setRows(5);
        jScrollPane2.setViewportView(editorerrores);

        jLabel1.setText("ERRORES");

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel2.setText("Descripcion de elementos");

        combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboActionPerformed(evt);
            }
        });

        jButton1.setText("Ver");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel3.setText("Detalles");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(combo, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addComponent(lblimagen, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel3))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lbldetalles, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lbl3, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(37, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(33, 33, 33)
                .addComponent(lblimagen, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(51, 51, 51)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(lbldetalles, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49))
        );

        jLabel4.setText("Tabla de Simbolos");

        editorsimbolos.setColumns(20);
        editorsimbolos.setRows(5);
        jScrollPane3.setViewportView(editorsimbolos);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1039, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 662, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 654, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 443, Short.MAX_VALUE))
                .addGap(2, 2, 2)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addGap(2, 2, 2)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jTabbedPane1.addTab("Editor de Juego", jPanel1);

        jMenu1.setText("Archivo");

        jMenuItem1.setText("Nuevo");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Abrir");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem3.setText("Guardar");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem4.setText("Guardar como");
        jMenu1.add(jMenuItem4);

        jMenuItem5.setText("Salir");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Ejecutar");

        jMenuItem6.setText("Compilar archivo");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem6);

        jMenuItem7.setText("Ejecutar juego");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem7);

        jMenuItem8.setText("Errores");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem8);

        jMenuItem9.setText("Tabla de simbolos");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem9);

        jMenuBar1.add(jMenu2);

        jMenu4.setText("Ayuda");

        jMenuItem10.setText("Manual de usuario");
        jMenu4.add(jMenuItem10);

        jMenuItem11.setText("Manual Tecnico");
        jMenu4.add(jMenuItem11);

        jMenuItem12.setText("Acerca de...");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem12);

        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1334, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("namee");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
combo.removeAllItems();
       // Personaje.Personajes.clear();
Objeto.Objetos.clear();
Fondo.Fondos.clear();
//Tablero.heroe=null;
Tablero.meta=null;
//Tablero.villanos.clear();
Tablero.ObjetosJuego.clear();
Tablero.fondoactual=null;
Tablero.villetiquetas.clear();
Tablero.ancho=0;
Tablero.alto=0;
Tablero.activo=true;
Tablero.list=null;
editor.setText("");
editorerrores.setText("");
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        leer(editor);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        enrutar();
        generarArchivo(editor);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
    System.exit(0);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
 crearArchivo(editor,"analizator.xconf");
  String[] archivoPrueba = {"analizator.xconf"};
  AnalizadorSintactico.main(archivoPrueba);
  editorerrores.setText("");
  if (!Errs.isEmpty())
  {
            String codigo="";
       for (ErroresC error:Errs)
       {
           if (error.tipo.equals("Lexico"))
       codigo+="Error "+error.tipo+" "+error.descripcion+" "+error.error+" Linea:"+error.fila+" Columna:"+error.columna+"\n";
           else if (error.tipo.equals("Sintactico"))
       codigo+="Error "+error.tipo+" Linea:"+error.fila+" Columna:"+error.columna+"\n";
           else
       codigo+="Error "+error.tipo+" "+error.descripcion+" "+error.error+"\n";

       }
        codigo+="* Compile nuevamente los archivos de configuracion y declaracion";
       editorerrores.setText(codigo);
       limpiartodo();
  }
  else
  {
      editorerrores.setText("Compilacion exitosa... 0 errores encontrados");
  }
  
  combo.removeAllItems();
       /*for (Personaje vill:Personaje.Personajes)
       {
       combo.addItem(vill.nombre);
       }
       for (Objeto ob:Objeto.Objetos)
       {
       combo.addItem(ob.nombre);
       }*/
    }//GEN-LAST:event_jMenuItem6ActionPerformed
void limpiartodo()
{
    Errs.clear(); 
//Tablero.heroe=null;
Tablero.ObjetosJuego.clear();
Tablero.meta=null;
Tablero.fondoactual=null;
//Tablero.villanos.clear();
//Personaje.Personajes.clear();
Fondo.Fondos.clear();
Objeto.Objetos.clear();
}
    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
editorerrores.setText("");
        if (Errs.isEmpty())
{
    if (Tablero.meta!=null && Tablero.fondoactual!=null)
    {
        editorerrores.setText("Compilacion exitosa... 0 errores encontrados");
            this.setVisible(true);
        Tablero nuevo= new Tablero();
        nuevo.setVisible(true);
    }
    
}
else
{
String codigo="";

for (ErroresC error:Errs)
{
    if (error.tipo.equals("Lexico"))
codigo+="Error "+error.tipo+" "+error.descripcion+" "+error.error+" Linea:"+error.fila+" Columna:"+error.columna+"\n";
    else if (error.tipo.equals("Sintactico"))
codigo+="Error "+error.tipo+" Linea:"+error.fila+" Columna:"+error.columna+"\n";
    else
codigo+="Error "+error.tipo+" "+error.descripcion+" "+error.error+"\n";
    
}
 codigo+="* Compile nuevamente los archivos de configuracion y declaracion";
editorerrores.setText(codigo);
limpiartodo();
}

        
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        String mensaje="";
        mensaje+="Nombre: Sergio Giovanni de León Torón \n";
        mensaje+="Carnet: 201314071 \n";
        mensaje+="Primer Proyecto, Organizacion de Lenguajes y Compiladores 1 \n";
        mensaje+="Aux. Bryan Valenzuela \n";
        JOptionPane.showMessageDialog(null,mensaje);
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        boolean found=false;
        /*for (Personaje vill:Personaje.Personajes)
       {
           if (found==false)
           {
                   if (vill.nombre.equals(combo.getSelectedItem().toString()))
                   {
                       lblimagen.setIcon (new javax.swing.ImageIcon(vill.PathImagen)); 
                       found=true;
                       
                       lbldetalles.setText("Descripcion: "+vill.descripcion+" \n");
                       lbl2.setText("Tipo: "+vill.tipo+" \n");
                       lbl3.setText("Vida: "+vill.vida+" \n");
                   }
             
           }
           
       }*/
       if (found==false)
       {
            for (Objeto ob:Objeto.Objetos)
            {
                if (found==false)
                   {
                   if (ob.nombre.equals(combo.getSelectedItem().toString()))
                   {
                       lblimagen.setIcon (new javax.swing.ImageIcon(ob.Path)); 
                       found=true;
                       
                       if (ob.tipo.equals("bonus"))
                           lbl2.setText("Bono: "+ob.creditos);
                       else if(ob.tipo.equals("estrella"))
                           lbl2.setText("Tiempo: "+ob.creditos);
                       
                       else if(ob.tipo.equals("veneno"))
                           lbl2.setText("Veneno: reiniciar");
                       
                       else if(ob.tipo.equals("bomba"))
                           lbl2.setText("Puntos: "+ob.creditos);
                       
                       else if(ob.tipo.equals("manzana"))
                           lbl2.setText("Puntos: "+ob.creditos);
                       
                       else if(ob.tipo.equals("bloque"))
                           lbl2.setText("Bloque ");
                       
                        lbl3.setText("");
                       lbldetalles.setText("Tipo: "+ob.tipo+" \n");
                   }
                   }

            }
       }
       
       
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem8ActionPerformed
static void TablaSimbolos(int op)
{
            String texto="";
        for (Fondo fond:Fondo.Fondos)
       {
           texto+="Fondo: "+fond.nombre+" Path: "+fond.PathFondo+"\n";
       }
        
        if (op==2)
        {
            //texto+="Personaje: "+Tablero.heroe.nombre+" Tipo: "+Tablero.heroe.tipo+" Vida: "+Tablero.heroe.vida+" Pos("+Tablero.heroe.PosX+","+Tablero.heroe.PosY+")"+"\n";
            //for (Personaje vil:Tablero.villanos)
       {
           
            //texto+="Personaje: "+vil.nombre+" Tipo: "+vil.tipo+" Vida: "+vil.vida+" Pos("+vil.PosX+","+vil.PosY+")"+"\n";
           
       }
            for (Objeto obs:Tablero.ObjetosJuego)
            {
            texto+="Objeto: "+obs.nombre+" Tipo: "+obs.tipo+" Pos("+obs.getX()+","+obs.getY()+")"+"\n";
            }
            
        }
      
           
editorsimbolos.setText(texto);
}
    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed

TablaSimbolos(1);
       
        
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    
    
    
    
    
    
public void leer(JTextArea cuerpo){
	        cuerpo.setText("");
	        try {
	            JFileChooser buscador = new JFileChooser();
	            buscador.showOpenDialog(buscador);
	            String contenido;
	           
	            String direc = buscador.getSelectedFile().getAbsolutePath();
	            FileReader doc =new FileReader(direc);
	            BufferedReader leer=new BufferedReader(doc);
	           
	            while((contenido=leer.readLine())!= null)
	            {
	             cuerpo.append(contenido); 
	             cuerpo.append(System.getProperty("line.separator"));
	           
	            }
	            doc.close();
	            leer.close();
                    
	        } catch (IOException ex) {
	            System.out.println("Error: "+ex);
	        }
                
	 }
public void crearArchivo(JTextArea cuerpo,String nombre){
            String contenido;
	    contenido=cuerpo.getText();
	    File f;
	    FileWriter escribir;
	    try{
	    f = new File(nombre);
	    escribir = new FileWriter(f);
	    BufferedWriter bw = new BufferedWriter(escribir);
	    PrintWriter pw = new PrintWriter(bw);
	    pw.write(contenido); 
	    pw.close();
	    bw.close();
         
             //JOptionPane.showMessageDialog(null,"El archivo se ha guardado exitosamente");
	    }
	    catch(IOException e){System.out.println("Error: "+e.getMessage());}
}
String ruta = "";
public void enrutar(){
	javax.swing.JFileChooser jF1= new javax.swing.JFileChooser(); 
        try{ 
	if(jF1.showSaveDialog(null)==jF1.APPROVE_OPTION){ 
	ruta = jF1.getSelectedFile().getAbsolutePath();  
	} 
	}catch (Exception ex){ 
	ex.printStackTrace(); 
	} 
	System.out.println(ruta);
	}
public void generarArchivo(JTextArea cuerpo){
	    String contenido;
	    contenido=cuerpo.getText();
	    File f;
	    FileWriter escribir;
	    try{
	    System.out.println(ruta);
	    f = new File(ruta+".xconf");
	    escribir = new FileWriter(f);
	    BufferedWriter bw = new BufferedWriter(escribir);
	    PrintWriter pw = new PrintWriter(bw);
	    pw.write(contenido); 
	    pw.close();
	    bw.close();
             JOptionPane.showMessageDialog(null,"Archivo Creado y Guardado");
	    }
	    catch(IOException e){System.out.println("Error: "+e.getMessage());}
	    }
    public static void main(String args[]) {
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormularioConfiguracion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormularioConfiguracion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormularioConfiguracion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormularioConfiguracion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>


        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormularioConfiguracion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox combo;
    private javax.swing.JTextArea editor;
    private javax.swing.JTextArea editorerrores;
    public static javax.swing.JTextArea editorsimbolos;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lbl2;
    private javax.swing.JLabel lbl3;
    private javax.swing.JLabel lbldetalles;
    private javax.swing.JLabel lblimagen;
    // End of variables declaration//GEN-END:variables
}
