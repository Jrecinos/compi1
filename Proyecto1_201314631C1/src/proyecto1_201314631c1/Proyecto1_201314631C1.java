/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto1_201314631c1;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
 import java.util.Random;
/**
 *
 * @author Dell
 */
public class Proyecto1_201314631C1 {

    /**
     * @param args the command line arguments
     */
     public final static int GENERAR = 1;
    public final static int EJECUTAR = 2;
    public final static int EJECUTAR2 = 3;
    public final static int SALIR = 4;
    
    public static void main(String[] args) {
         java.util.Scanner in = new Scanner(System.in);
        int valor = 0;
        Random rnd = new Random();
        do {
             
             System.out.println("Numero Aleatorio: "+(rnd.nextInt(2)+1));
            System.out.println("Elija una opcion: ");
            System.out.println("1) Generar");
            System.out.println("2) Ejecutar");
            System.out.println("3) Ejecutar2");
            System.out.println("4) Salir");
            System.out.print("Opcion: ");
            valor = in.nextInt();
            switch (valor) {
                
                case GENERAR: {
                    
                    
                    
                    System.out.println("\n*** Generando ***\n");
                    String archLexico = "";
                    String archSintactico = "";
                    if (args.length > 0) {
                        System.out.println("\n*** Procesando archivos custom ***\n");
                        archLexico = args[0];
                        archSintactico = args[1];
                    } else {
                        System.out.println("\n*** Procesando archivo default ***\n");
                        archLexico = "scanerP.flex";
                        archSintactico = "parserP.cup"; 
                        
                        
                    }
                    String[] alexico = {archLexico};
                    String[] asintactico = {"-parser", "AnalizadorSintactico", archSintactico};
                    jflex.Main.main(alexico);
                    
                    try {
                        java_cup.Main.main(asintactico);
                    } catch (Exception ex) {
                        Logger.getLogger(Proyecto1_201314631C1.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //movemos los archivos generados
                    boolean mvAL = moverArch("AnalizadorLexico.java");
                    boolean mvAS = moverArch("AnalizadorSintactico.java"); 
                  
                    boolean mvSym= moverArch("sym.java");
                    if(mvAL && mvAS && mvSym){
                        System.exit(0);
                    }
                    System.out.println("Generado!");
                    break;
                }
                case EJECUTAR: {
                   
                    String[] archivoPrueba = {"Archivo1_2.xconf"};
                   // java_cup.runtime.Scanner( new FileReader(archivoPrueba[0]));
                    
                    AnalizadorSintactico.main(archivoPrueba);
                    System.out.println("Ejecutado!");
                    
                   
                    for (Objeto o:Objeto.Objetos)
			{
			System.out.println("Objeto: "+o.nombre+" Tipo: "+o.tipo);
			}
                    for (Fondo f:Fondo.Fondos)
			{
			System.out.println("Fondo: "+f.nombre+" Path: "+f.PathFondo);
			}
                    break;
                    
                }
                case EJECUTAR2: {
                    System.out.println("si entra");
                    String[] archivoPrueba = {"Archivo2_2.xconf"};
                   // java_cup.runtime.Scanner( new FileReader(archivoPrueba[0]));
                    
                    AnalizadorSintactico.main(archivoPrueba);
                    System.out.println("Ejecutado archivo 2!");
                    
                    
                    
                    for (Objeto o:Tablero.ObjetosJuego)
			{
			System.out.println("Nombre: "+o.nombre+"("+o.getX() + " , "+o.getY()+")");
			}
                    if (Tablero.fondoactual!= null)
                        System.out.println("Fondo es:"+Tablero.fondoactual.nombre+ "Path "+Tablero.fondoactual.PathFondo);
                        System.out.println("Ancho= "+Tablero.ancho+ " Alto="+Tablero.alto);
                    break;
                    
                }
                case SALIR: {
                    System.out.println("Adios!");
                    break;
                }
                default: {
                    System.out.println("Opcion no valida!");
                    break;
                }
            }
        } while (valor != 3);
        
    }
    
     public static boolean moverArch(String archNombre) {
        boolean efectuado = false;
        File arch = new File(archNombre);
        if (arch.exists()) {
            System.out.println("\n*** Moviendo " + arch + " \n***");
            Path currentRelativePath = Paths.get("");
            String nuevoDir = currentRelativePath.toAbsolutePath().toString()
                    + File.separator + "src" + File.separator
                    + "ejemplocup" + File.separator + arch.getName();
            File archViejo = new File(nuevoDir);
            archViejo.delete();
            if (arch.renameTo(new File(nuevoDir))) {
                System.out.println("\n*** Generado " + archNombre + "***\n");
                efectuado = true;
            } else {
                System.out.println("\n*** No movido " + archNombre + " ***\n");
            }

        } else {
            System.out.println("\n*** Codigo no existente ***\n");
        }
        return efectuado;
    }
    
    
}
