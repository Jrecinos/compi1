package proyecto1_201314631c1;

import java.util.ArrayList;
public class Objeto {
    public static ArrayList<Objeto>Objetos= new ArrayList<Objeto>();
    String nombre;
    String tipo;
    String Path;
    int creditos;
    int destruir;
    private int PosX;
    private int PosY;
    
    public Objeto(String nombre, String Path, String tipo,int CreditosDestruir)
    {
    this.nombre=nombre;
    this.Path=Path;
    this.tipo=tipo;
    if (tipo.equals("bonus"))
        this.creditos=CreditosDestruir;
    else
        this.destruir=CreditosDestruir;
    
    }
    
    public static void AsignSetXY(String nombre, int x, int y)
    {
        boolean encontrado=false;
                        for (Objeto o:Objeto.Objetos)
			{
			if (o.nombre.equals(nombre))
                        {
                            encontrado=true;
                            int creddest;
                            if (o.tipo.equals("bonus"))
                                creddest=o.creditos;
                            else
                                creddest=o.destruir;
                            Objeto nuevo = new Objeto(o.nombre,o.Path,o.tipo,creddest);
                            nuevo.setX(x);
                            nuevo.setY(y);
                            if (o.tipo.equals("meta"))
                            {
                                Tablero.meta=nuevo;
                            }
                            else
                            {
                                Tablero.ObjetosJuego.add(nuevo);
                            }
                            
                        }
			}
                        if (encontrado==false)
                        {
                        ErroresC nuevo= new ErroresC("Objeto no ha sido declarado",nombre,"Semantico");
                        FormularioConfiguracion.Errs.add(nuevo);
                        }
    }
    int getX()
    {
    return this.PosX;
    }
    int getY()
    {
    return this.PosY;
    }
    void setX(int x)
    {
        this.PosX=x;
    }
    void setY(int y)
    {
        this.PosY=y;
    }
}
